# CI Automation 

A collection of Docker images to automate common CI pipeline tasks.

## Overview

| Image | Base image | Tools | Supported platforms |
| --- | --- | --- | --- |
| kubetools | [docker.io/library/alpine](https://hub.docker.com/_/alpine) | `kubectl`, `helm`, `envsubst` | `linux/arm/v7`, `linux/arm64/v8`, `linux/amd64` |
| ansible | [docker.io/library/python:slim](https://hub.docker.com/_/python) | `ansible`, `ssh` | `linux/amd64` |

## License

This project is licensed under the terms of the [MIT license](./LICENSE.md).
